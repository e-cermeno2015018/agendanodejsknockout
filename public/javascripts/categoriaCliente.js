var CategoriaCliente = function(){
  var main = this;
  var categoriaUri = "http://localhost:3000/api/categoria/";
  var categoriaUri1 = "http://localhost:3000/api/categoria";


    main.categoria = ko.observableArray([]);
    main.error = ko.observable();
    main.categoriaCargada = ko.observable();

  function ajaxHelper(uri, method, data) {
    return $.ajax({
      url : uri,
      type: method,
      dataType: 'json',
      contentType: 'application/json',
      data: data ? JSON.stringify(data) : null
    }).fail(function(jqXHR, textStatus, errorThrown){
      main.error(errorThrown);
    })
  }

  main.categoriaNueva = {
    nombreCategoria: ko.observable()
  }

  main.agregarCategoria = function (formElement) {
      var nuevaCategoria = {
          nombreCategoria: main.categoriaNueva.nombreCategoria()
      }
      console.log(JSON.stringify(nuevaCategoria));
      ajaxHelper(categoriaUri, 'POST', nuevaCategoria)
          .done(function (data) {
            main.obtenerCategoria();
          });
  }
  main.obtenerCategoria = function() {
          ajaxHelper(categoriaUri, 'GET').done(function(data) {
            main.categoria(data);
          });
        }
  main.obtenerCategoria();
}

$(document).ready(function() {
  var categoria = new CategoriaCliente();
  ko.applyBindings(categoria);
});
