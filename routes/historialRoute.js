var express = require('express');
var historial = require('../model/historial');
var Autenticacion = require('../helper/autenticacion');
var router = express.Router();
var auth = new Autenticacion();

router.get('/api/historial/', function(req, res) {
  auth.autorizar(req);
  if(auth.getAcceso()) {
    historial.selectAll(auth.getIdUsuario(), function(error, resultados){
      if(typeof resultados !== undefined) {
        res.render('dashboard/historial', {historial});
      } else {
        res.json({"Mensaje": "No hay contactos"});
      }
    });
  } else {
    res.redirect('/autenticar');
  }
});
