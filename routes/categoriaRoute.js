var express = require('express');
var categoria = require('../model/categoria');
var router = express.Router();

router.get('/api/categoria/', function(req, res) {
  categoria.selectAll(function(error, result){
    if (typeof result !== undefined) {
      res.json(result);
    } else {
      res.json({"Mensaje": "No hay categorias"});
    }
  });
});

router.get('api/categoria/:idCategoria', function(req, res) {
  var idCategoria = req.params.idCategoria;
  categoria.delete(idCategoria, function(error, resultado){
    if (typeof resultado !== undefined) {
      categoria.selectAll(function(error, result){
        if (typeof resultados !== undefined) {
          res.render('listaCategorias', {result})
        } else {
          res.json({"Mensaje": "No hay categorias"});
        }
      });
    } else {
      res.json({"Mensaje": "No hay categorias"});
    }
  });
});

categoria.update = function(data, callback){
  if(database){
    var sql = "UPDATE Categoria SET nombreCategoria = ? WHERE idCategoria = ?";
    database.query(sql, [data.nombreCategoria, data.idCategoria], function(error, resultado){
      if(error){
        throw error;
      } else {
        callback(null, data);
      }
    });
  }
}

router.post('/api/categoria', function(req, res) {
  var data ={
    idCategoria: null,
    nombreCategoria: req.body.nombreCategoria
  }
  categoria.insert(data, function(error, resultado){
    if (resultado && resultado.insertId >0) {
      res.redirect('/dashboard/listaCategorias');
    } else {
      res.json({"Mensaje": "No se inserto la categoria"});
    }
  });
});

router.delete('/api/categoria/:idCategoria', function(req, res) {
    var idCategoria = req.params.idCategoria;
    var ids = {
      idUsuario : auth.getIdUsuario(),
      idCategoria: idCategoria
    }
    contacto.delete(ids, function(error, resultado){
      if(resultado) {
        res.json(resultado);
      } else {
        res.json({"Mensaje": "No se puede eliminar"});
      }
  });
});

router.put('/api/categoria/:idCategoria', function(req, res) {
  var idCategoria = req.params.idCategoria;
  var data = {
    idCategoria: req.body.idCategoria,
    nombreCategoria: req.body.nombreCategoria
  }
  if(idCategoria === data.idCategoria){
    categoria.update(data, function(error, resultado) {
      if(resultado !== undefined){
        res.redirect("/api/categoria");
      } else {
        res.json({"Mensaje" : "No se modifico la categoria"});
      }
    });
  } else {
    res.json({"Mensaje" : "No concuerdan los ids"});
  }
});

module.exports = router;
