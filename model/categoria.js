var database = require('./database');
var categoria = {};
categoria.selectAll = function(callback) {
  if(database) {
    database.query("SELECT * FROM Categoria",
    function(error, result) {
      if(error) {
        throw error;
      } else {
        callback(null, result);
      }
    });
  }
}

categoria.select = function(idCategoria, callback) {
  if(database) {
    var consulta = "SELECT * FROM Categoria WHERE idCategoria = ?";
    database.query(consulta,idCategoria, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });
  }
}

categoria.insert = function(data, callback){
  if(database){
    database.query("CALL SP_InsertarCategoria(?)", [data.nombreCategoria], function(error, resultado){
      if(error){
        throw error;
      } else {
        callback(null, {"insertId" : resultado.insertId});
      }
    });
  }
}

categoria.update = function(data, callback) {
  if(database) {
    var sql = "CALL SP_ActualizarCategoria(?,?)";
    database.query(sql, [data.nombreCategoria, data.idCategoria],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, data);
      }
    });
  }
}

categoria.delete = function(data, callback) {
  if(database) {
    database.query("CALL SP_BorrarCategoria(?,?)",[data.idCategoria],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });
  }
}

module.exports = categoria;
