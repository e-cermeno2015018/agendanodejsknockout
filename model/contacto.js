var database = require('./database');
var contacto = {};
var Autenticacion = require('../helper/autenticacion');
var auth = new Autenticacion();

contacto.selectAll = function(idUsuario, callback) {
  if(database) {
    database.query("SELECT contacto.idContacto, nombre, apellido, direccion, telefono, correo, nombreCategoria FROM usuarioDetalle INNER JOIN contacto on usuarioDetalle.idContacto = contacto.idContacto INNER JOIN categoria ON contacto.idCategoria = categoria.idCategoria  where idUsuario = ?",
    idUsuario,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });
  }
}

contacto.selectHistorial = function(idUsuario, callback){
  if(database) {
    database.query("SELECT * FROM Historial WHERE idUsuario = ?", idUsuario,
    function(error, resultados) {
      if(error){
        throw error;
      } else{
        callback(null, resultados)
      }
    });
  }
}

contacto.select = function(idContacto, callback) {
  if(database) {
    var sql = "SELECT * FROM Contacto WHERE idContacto = ?";
    database.query(sql, idContacto,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });
  }
}

contacto.insert = function(idUsuario, data, callback) {
  if(database) {
    database.query("CALL SP_InsertarContactoUsuario(?, ?, ?, ?, ?, ?, ?)", [data.nombre, data.apellido, data.direccion, data.telefono, data.correo, data.idCategoria, idUsuario],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });
  }
}

contacto.update = function(id2, id, data, callback) {
  if(database) {
    var sql = "CALL SP_ActualizarContacto(?,?,?,?,?,?,?,?)";
    database.query(sql, [data.nombre, data.apellido, data.direccion, data.telefono, data.correo, data.idCategoria, id, id2],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, data);
      }
    });
  }
}

contacto.delete = function(ids, callback) {
  if(database) {
    database.query("CALL SP_BorrarContactoUsuario(?,?)",[ids.idUsuario, ids.idContacto],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });
  }
}

module.exports = contacto;
